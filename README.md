# US_HealthCare_App

I'm excited to share about my recent project! I built a random forest model regressor using scikit-learn to predict healthcare costs in the US. It was a challenging but rewarding experience to develop and fine-tune the model using various data preprocessing techniques and hyperparameter tuning. To make it accessible to a wider audience, I used Django to create a user-friendly website where anyone can input their healthcare data and get a predicted cost. It was fulfilling to see the model's accuracy improve with each iteration and to witness the potential impact it could have on individuals and healthcare providers. Overall, this project was a great opportunity for me to enhance my data science, web development and debugging skills.

# Results

Below shows the final product. Figure 1 shows me entering my own stats and Figure 2 shows the regression model result.

![alt text](us_healthcare_image.PNG)

Figure 1: Website displaying input boxes to enter metrics that heavily affect healthcare costs

![alt text](us_healthcare_image_result.PNG)

Figure 2: Output of the algorithm shown on website
